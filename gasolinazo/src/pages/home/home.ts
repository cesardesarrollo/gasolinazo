import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AngularFire, AuthProviders, AuthMethods } from 'angularfire2';
import { Http } from '@angular/http';
import { MapPage } from '../map/map';
import { OptionsPage } from '../options/options';
import { ModalController, PopoverController, NavParams } from 'ionic-angular';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  //@ViewChild('popoverContent', { read: ElementRef }) 
  content: ElementRef;
  //@ViewChild('popoverText', { read: ElementRef }) 
  text: ElementRef;

  displayName;
  photoURL;
  email;
  password;
  error;
  uid;

  constructor(public navCtrl: NavController, private af: AngularFire, private http: Http, public modalCtrl: ModalController, private popoverCtrl: PopoverController) {
    
  }
  ngOnInit() {
    this.af.auth.subscribe(authState => {
      if(!authState) {
        console.log("NOT LOGGED")
        this.displayName = null;
        this.photoURL = null;
        return;
      }

      console.log("LOGGED IN, AUTHSTATE", authState)
      
      // Aquí es donde puedo ver que permisos tiene
      let userRef = this.af.database.object("/users/" + authState.uid)
      userRef.subscribe(user => {
        if(authState.facebook){
          /* Esto solo para facebook authentication */
          let url = `https://graph.facebook.com/v2.8/${authState.facebook.uid}?fields=first_name,last_name&access_token=${user.accessToken}`;
          this.http.get(url).subscribe(response =>{
            let user = response.json();
            userRef.update({
              firstName: user.first_name,
              lastName: user.last_name
            });
          });
        }
      });

      this.displayName = authState.auth.displayName;
      this.photoURL = authState.auth.photoURL;
    });
  }

  login_facebook() {
    this.af.auth.login({
      provider: AuthProviders.Facebook,
      method: AuthMethods.Popup,
      scope: ['public_profile', 'user_friends']
    }).then((authState: any) => {
      console.log("AFTER LOGIN", authState);
      this.af.database.object('/users/' + authState.uid).update({
        accessToken: authState.facebook.accessToken
      });
    });
  }

  logout() {
    this.af.auth.logout();
  }

  register(){
    this.af.auth.createUser({
      email: this.email,
      password: this.password
    })
    .then(authState => {
      console.log("REGISTER-THEN", authState)
      authState.auth.sendEmailVerification();
    })
    .catch(error => this.error = error.message);
  }

  login() {
    this.af.auth.login({
      email: this.email,
      password: this.password
    }, {
      method: AuthMethods.Password,
      provider: AuthProviders.Password

    })
    .then(authState => this.uid = authState.uid)
    .catch(error => this.error = error.message)

  }

  goToMapPage() {
    this.navCtrl.push(MapPage, {item: 1});
  }

  presentModal() {
    let modal = this.modalCtrl.create(OptionsPage);
    modal.present();
  }


  presentPopover(ev) {

    let popover = this.popoverCtrl.create(OptionsPage, {
      contentEle: this.content.nativeElement,
      textEle: this.text.nativeElement
    });

    popover.present({
      ev: ev
    });
}



}