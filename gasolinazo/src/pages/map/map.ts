

import { Component, NgZone } from '@angular/core';
import { NavController,Platform } from 'ionic-angular';
import { ViewController  } from 'ionic-angular';
import {
  Geolocation,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapsLatLng,
  CameraPosition,
  GoogleMapsMarkerOptions,
  GoogleMapsMarker
} from 'ionic-native';

@Component({
  selector: 'page-map',
  templateUrl: 'map.html'
})
export class MapPage {
  private map: GoogleMap;
  posicion: any;

  
  constructor(public viewCtrl: ViewController, public navCtrl: NavController, private platform: Platform, private _zone :NgZone) {
    platform.ready().then(() => this.onPlatformReady());
  }

  private onPlatformReady(): void { 
  }

  ngAfterViewInit() {
    this.loadMap();
  }

  loadMap() {
      this.map = new GoogleMap("map-canvas");

      // listen to MAP_READY event
      this.map.one(GoogleMapsEvent.MAP_READY).then(() => {
          this._zone.run(() => {
              //Geolocation.getCurrentPosition().then(pos => this.posicionar(pos));
              this.posicionar();

          });
      });
  }

  private putStations(){


      // Posiciones de gasolineras
      let gasolineras: any[] = [
          {
              title : 'Gasolinera 1',
              snippet : 'Magna: $16.32 | Premium: $18.34 | Rating: 4.5',
              lat : 20.0000910,
              lng : -103.0005010
          },
          {
              title : 'Gasolinera 2',
              snippet : 'Magna: $16.52 ',
              lat : 20.1000910,
              lng : -103.0007010
          },
          {
              title : 'Gasolinera 3',
              snippet : 'Magna: $16.98',
              lat : 20.2000910,
              lng : -103.0009010
          }
      ];

      for (let index in gasolineras) {

          let gasolinera = gasolineras[index];
          gasolinera['markerIndex'] = parseInt(index);

          this.map.addMarker({
              'position': new GoogleMapsLatLng(gasolinera['lat'], gasolinera['lng']),
              'title': gasolinera['title'],
              'snippet':  gasolinera['snippet'],
              'markerClick': function(marker) {
                  alert(JSON.stringify(gasolinera));
              },
              'icon': {
                'url': 'http://icons.iconarchive.com/icons/icons8/windows-8/256/City-Gas-Station-icon.png',
                'anchor': [14, 28],
                'size': {
                  width: 14,
                  height: 28
                }
              }
          }).then((marker: GoogleMapsMarker) => {
              marker.showInfoWindow();
          });

      }
    


  }

  private posicionar(): void {

        this.putStations();

        Geolocation.watchPosition().subscribe(pos => {

            // Posición del usuario
            this.posicion = JSON.stringify(pos);

            // create LatLng object
            let myposition: GoogleMapsLatLng = new GoogleMapsLatLng(pos.coords.latitude,pos.coords.longitude);

            this.map.animateCamera({ target: myposition, zoom: 12, tilt: 30 });
            
            // create new marker
            let markerOptions: GoogleMapsMarkerOptions = {
                position: myposition,
                title: 'Aquí estoy yo'
            };

            this.map.addMarker(markerOptions)
                .then((marker: GoogleMapsMarker) => {
                    marker.showInfoWindow();
                });
        });

    
  }
  
  dismiss() {
      this.viewCtrl.dismiss();
  }


}
