import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { AngularFireModule } from 'angularfire2';
import { MapPage } from '../pages/map/map';
import { OptionsPage } from '../pages/options/options';


export const firebaseConfig = {
    apiKey: "AIzaSyANP9cFDByW8SGgbKdNdwGKRqOKqbS27IA",
    authDomain: "gas1-be5c6.firebaseapp.com",
    databaseURL: "https://gas1-be5c6.firebaseio.com",
    storageBucket: "gas1-be5c6.appspot.com",
    messagingSenderId: "435608044463"
}

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    MapPage,
    OptionsPage
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    MapPage,
    OptionsPage
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}]
})
export class AppModule {}
