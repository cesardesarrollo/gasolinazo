
import { Component, ViewChild } from '@angular/core';
import { ViewController, List } from 'ionic-angular';


@Component({
    templateUrl: 'options.html'
})
export class OptionsPage {
  @ViewChild(List) list: List;

    constructor(  public viewCtrl: ViewController ){
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }
    stopSliding() {
        //this.list.enableSlidingItems(false);
    }
}